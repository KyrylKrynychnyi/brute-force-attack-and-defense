import requests

# def request(login, password):
#     response = requests.post('http://127.0.0.1:4000/auth', json={'login': login, 'password': password})
#     return response.status_code == 200


class Request:
    def __init__(self):
        self.ip_addr = 'http://127.0.0.1:4000/auth'

    def request(self, login, password):
        response = requests.post(self.ip_addr, json={'login': login, 'password': password})
        return response.status_code == 200

    def get_ip_addr(self):
        return self.ip_addr