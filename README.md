# Brute force attack and defense

This is my first try to create a brute force attack and protection from it.
It's quite simple. This project uses Python and Nginx.

## How to run

Open hack.py and specify login and password generators and request methods. Pass these arguments to `class Hack`.

`python hack.py`

## API requesters  

To add requester, you have to create a file in the requesters package and
create here a function

`request(login, password): return bool`

## API generators

To add a generator, you have to create a file in the generators package and
create there a class:

```
class Generator:

    def next(self):
        return str or None
```
